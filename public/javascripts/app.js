(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return ({}).hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';

    if (has(cache, path)) return cache[path].exports;
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex].exports;
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  var define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  var list = function() {
    var result = [];
    for (var item in modules) {
      if (has(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  globals.require = require;
  globals.require.define = define;
  globals.require.register = define;
  globals.require.list = list;
  globals.require.brunch = true;
})();
require.register("application", function(exports, require, module) {
var Application,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Application = (function(_super) {
  __extends(Application, _super);

  function Application() {
    this.initialize = __bind(this.initialize, this);
    return Application.__super__.constructor.apply(this, arguments);
  }

  Application.prototype.initialize = function() {
    this.on("start", (function(_this) {
      return function(options) {
        Backbone.history.start();
        return typeof Object.freeze === "function" ? Object.freeze(_this) : void 0;
      };
    })(this));
    this.addInitializer((function(_this) {
      return function(options) {
        var AppLayout;
        AppLayout = require('views/AppLayout');
        _this.layout = new AppLayout();
        return _this.layout.render();
      };
    })(this));
    this.addInitializer((function(_this) {
      return function(options) {
        var Router;
        Router = require('lib/router');
        return _this.router = new Router();
      };
    })(this));
    return this.start();
  };

  return Application;

})(Backbone.Marionette.Application);

module.exports = new Application();
});

;require.register("initialize", function(exports, require, module) {
var application;

application = require('application');

$(function() {
  return application.initialize();
});
});

;require.register("lib/data_helpers", function(exports, require, module) {
var TreeNode, TreeNodeCollection, data, getColor, getTree, loadTree, saveTree, tree;

TreeNode = require('models/node').TreeNode;

TreeNodeCollection = require('models/node').TreeNodeCollection;

data = [['TURQUOISE', '#1ABC9C'], ['GREEN SEA', '#16A085'], ['EMERALD', '#2ECC71'], ['NEPHRITIS', '#27AE60'], ['PETER RIVER', '#3498DB'], ['BELIZE HOLE', '#2980B9'], ['AMETHYST', '#9B59B6'], ['WISTERIA', '#8E44AD'], ['WET ASPHALT', '#34495E'], ['MIDNIGHT BLUE', '#2C3E50'], ['SUN FLOWER', '#F1C40F'], ['ORANGE', '#F39C12'], ['CARROT', '#E67E22'], ['PUMPKIN', '#D35400'], ['ALIZARIN', '#E74C3C'], ['POMEGRANATE', '#C0392B'], ['CLOUDS', '#ECF0F1'], ['SILVER', '#BDC3C7'], ['CONCRETE', '#95A5A6'], ['ASBESTOS', '#7F8C8D']];

tree = null;

getTree = function() {
  return tree = loadTree();
};

saveTree = function() {
  var m, saveData, saveString, _i, _len, _ref;
  saveData = [];
  _ref = tree.models;
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    m = _ref[_i];
    saveData.push(m.getSaveData());
  }
  saveString = JSON.stringify(saveData);
  return localStorage.setItem('NestedList', saveString);
};

loadTree = function() {
  var collection, createModel, jsonData, obj, saveString, _i, _len;
  saveString = localStorage.getItem('NestedList');
  jsonData = JSON.parse(saveString);
  createModel = function(data) {
    return new TreeNode(data);
  };
  collection = new TreeNodeCollection([]);
  if (jsonData) {
    for (_i = 0, _len = jsonData.length; _i < _len; _i++) {
      obj = jsonData[_i];
      collection.add(createModel(obj));
    }
  }
  return collection;
};

getColor = function() {
  return data[Math.floor(Math.random() * data.length)];
};

module.exports = {
  getTree: getTree,
  saveTree: saveTree,
  getColor: getColor
};
});

;require.register("lib/router", function(exports, require, module) {
var HeaderView, Router, TreeNodeCollection, TreeRootView, application, helpers,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

application = require('application');

helpers = require('lib/data_helpers');

HeaderView = require('views/HeaderView');

TreeRootView = require('views/TreeRootView');

TreeNodeCollection = require('models/node').TreeNodeCollection;

module.exports = Router = (function(_super) {
  __extends(Router, _super);

  function Router() {
    this.home = __bind(this.home, this);
    return Router.__super__.constructor.apply(this, arguments);
  }

  Router.prototype.routes = {
    '': 'home'
  };

  Router.prototype.home = function() {
    var headerView, treeRootView, vent;
    vent = new Backbone.Wreqr.EventAggregator();
    headerView = new HeaderView({
      vent: vent
    });
    application.layout.header.show(headerView);
    treeRootView = new TreeRootView({
      vent: vent,
      collection: helpers.getTree()
    });
    application.layout.content.show(treeRootView);
    return vent.on('top:node:added', function() {
      return treeRootView.addTopLevelItem();
    });
  };

  return Router;

})(Backbone.Router);
});

;require.register("models/node", function(exports, require, module) {
var TreeNode, TreeNodeCollection;

TreeNode = Backbone.Model.extend({
  localStorage: new Backbone.LocalStorage('NestedList'),
  defaults: {
    nodes: []
  },
  initialize: function() {
    var nodes;
    nodes = this.get('nodes');
    if (nodes) {
      this.nodes = new TreeNodeCollection(nodes);
      return this.unset('nodes');
    } else {
      return this.nodes = [];
    }
  },
  getSaveData: function() {
    var data, n, nodes, _i, _len, _ref;
    nodes = [];
    _ref = this.nodes.models;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      n = _ref[_i];
      nodes.push(n.getSaveData());
    }
    return data = {
      nodeName: this.get('nodeName'),
      nodeColor: this.get('nodeColor'),
      nodes: nodes
    };
  }
});

TreeNodeCollection = Backbone.Collection.extend({
  model: TreeNode
});

({
  localStorage: new Backbone.LocalStorage('NestedList')
});

module.exports = {
  TreeNode: TreeNode,
  TreeNodeCollection: TreeNodeCollection
};
});

;require.register("views/AppLayout", function(exports, require, module) {
var AppLayout, application,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

application = require('application');

module.exports = AppLayout = (function(_super) {
  __extends(AppLayout, _super);

  function AppLayout() {
    return AppLayout.__super__.constructor.apply(this, arguments);
  }

  AppLayout.prototype.template = 'views/templates/appLayout';

  AppLayout.prototype.el = "body";

  AppLayout.prototype.regions = {
    header: "#header",
    content: "#content"
  };

  return AppLayout;

})(Backbone.Marionette.LayoutView);
});

;require.register("views/HeaderView", function(exports, require, module) {
var HeaderView,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

module.exports = HeaderView = (function(_super) {
  __extends(HeaderView, _super);

  function HeaderView() {
    this._addNode = __bind(this._addNode, this);
    return HeaderView.__super__.constructor.apply(this, arguments);
  }

  HeaderView.prototype.id = 'home-view';

  HeaderView.prototype.template = 'views/templates/header';

  HeaderView.prototype.events = {
    'click .btn-success': '_addNode'
  };

  HeaderView.prototype.initialize = function(options) {
    return this.vent = options.vent;
  };

  HeaderView.prototype._addNode = function(e) {
    e.stopPropagation();
    return this.vent.trigger('top:node:added');
  };

  return HeaderView;

})(Backbone.Marionette.ItemView);
});

;require.register("views/TreeRootView", function(exports, require, module) {
var TreeNode, TreeRootView, TreeView, helpers,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TreeView = require('views/TreeView');

TreeNode = require('models/node').TreeNode;

helpers = require('lib/data_helpers');

module.exports = TreeRootView = (function(_super) {
  __extends(TreeRootView, _super);

  function TreeRootView() {
    return TreeRootView.__super__.constructor.apply(this, arguments);
  }

  TreeRootView.prototype.tagName = 'ul';

  TreeRootView.prototype.className = 'list-group';

  TreeRootView.prototype.childView = TreeView;

  TreeRootView.prototype.itemView = TreeView;

  TreeRootView.prototype.addTopLevelItem = function() {
    var item, node;
    item = helpers.getColor();
    node = new TreeNode({
      nodeName: item[0],
      nodeColor: item[1],
      nodes: []
    });
    this.collection.add(node);
    return helpers.saveTree();
  };

  return TreeRootView;

})(Backbone.Marionette.CollectionView);
});

;require.register("views/TreeView", function(exports, require, module) {
var TreeNode, TreeView, helpers,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TreeNode = require('models/node').TreeNode;

helpers = require('lib/data_helpers');

module.exports = TreeView = (function(_super) {
  __extends(TreeView, _super);

  function TreeView() {
    this.onRender = __bind(this.onRender, this);
    this._removeNode = __bind(this._removeNode, this);
    this._addNode = __bind(this._addNode, this);
    return TreeView.__super__.constructor.apply(this, arguments);
  }

  TreeView.prototype.tagName = 'li';

  TreeView.prototype.className = 'list-group-item';

  TreeView.prototype.template = 'views/templates/node';

  TreeView.prototype.events = {
    'click .btn-success': '_addNode',
    'click .btn-danger': '_removeNode'
  };

  TreeView.prototype.initialize = function() {
    return this.collection = this.model.nodes;
  };

  TreeView.prototype._addNode = function(e) {
    var item, node;
    e.stopPropagation();
    item = helpers.getColor();
    node = new TreeNode({
      nodeName: item[0],
      nodeColor: item[1],
      nodes: []
    });
    this.collection.add(node);
    return helpers.saveTree();
  };

  TreeView.prototype._removeNode = function(e) {
    e.stopPropagation();
    this.model.destroy();
    return helpers.saveTree();
  };

  TreeView.prototype.appendHtml = function(collectionView, itemView) {
    return collectionView.$('li:first').append(itemView.el);
  };

  TreeView.prototype.onRender = function() {
    if (_.isUndefined(this.collection)) {
      return this.$("ul:first").remove();
    }
  };

  return TreeView;

})(Backbone.Marionette.CompositeView);
});

;require.register("views/templates/appLayout", function(exports, require, module) {
var __templateData = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div id=\"header\" class=\"container\"></div>\n<div id=\"content\" class=\"container\"></div>\n";
  });
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;require.register("views/templates/header", function(exports, require, module) {
var __templateData = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<button type=\"button\" class=\"btn btn-default btn-sm btn-success\" aria-label=\"Left Align\">\n	<span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span>\n</button>\n";
  });
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;require.register("views/templates/node", function(exports, require, module) {
var __templateData = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div style=\"background-color:";
  if (helper = helpers.nodeColor) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.nodeColor); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">\n	<button type=\"button\" class=\"btn btn-default btn-success\" aria-label=\"Left Align\">\n		<span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span>\n	</button>\n	<button type=\"button\" class=\"btn btn-default btn-danger\" aria-label=\"Left Align\">\n		<span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span>\n	</button>\n	";
  if (helper = helpers.nodeName) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.nodeName); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\n</div>\n";
  return buffer;
  });
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;
//# sourceMappingURL=app.js.map