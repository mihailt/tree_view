# Tree view with Marionette

## Build details
Current Build is available in /public folder

## Development
To edit you need to have brunch installed
after run `npm install` and `bower install`
and launch `brunch watch --server`

## Other
Currently backbone.localstorage is not used due to some errors,
rolled custom save load methods instead,
need to spend some time to see what is happening there.
