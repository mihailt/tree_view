module.exports = class HeaderView extends Backbone.Marionette.ItemView
	id: 'home-view'
	template: 'views/templates/header'
	events:
		'click .btn-success': '_addNode'
	initialize: (options) ->
		@vent = options.vent

	_addNode: (e) =>
		e.stopPropagation()
		@vent.trigger 'top:node:added'
