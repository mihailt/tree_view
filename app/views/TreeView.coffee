TreeNode = require('models/node').TreeNode
helpers = require 'lib/data_helpers'

module.exports = class TreeView extends Backbone.Marionette.CompositeView
	tagName: 'li'
	className: 'list-group-item'
	template: 'views/templates/node'
	events:
		'click .btn-success': '_addNode'
		'click .btn-danger': '_removeNode'

	initialize: () ->
		@collection = @model.nodes;

	_addNode: (e) =>
		e.stopPropagation()
		item = helpers.getColor()
		node = new TreeNode
			nodeName: item[0]
			nodeColor: item[1]
			nodes:[]
		@collection.add node
		helpers.saveTree()

	_removeNode: (e) =>
		e.stopPropagation()
		@model.destroy()
		helpers.saveTree()

	appendHtml: (collectionView, itemView) ->
		collectionView.$('li:first').append itemView.el

	onRender: =>
		if _.isUndefined @collection
			@$("ul:first").remove()
