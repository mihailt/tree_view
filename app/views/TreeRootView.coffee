TreeView = require('views/TreeView')
TreeNode = require('models/node').TreeNode
helpers = require 'lib/data_helpers'

module.exports = class TreeRootView extends Backbone.Marionette.CollectionView
	tagName: 'ul'
	className: 'list-group'
	childView: TreeView
	itemView: TreeView

	addTopLevelItem: () ->
		item = helpers.getColor()
		node = new TreeNode
			nodeName: item[0]
			nodeColor: item[1]
			nodes:[]
		@collection.add node
		helpers.saveTree()
