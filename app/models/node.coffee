TreeNode = Backbone.Model.extend
	localStorage: new Backbone.LocalStorage 'NestedList'
	defaults:
		nodes: []
	initialize: ()->
		nodes = @get 'nodes'
		if nodes
			@nodes = new TreeNodeCollection nodes
			@unset 'nodes'
		else
			@nodes = []

	#custom hack because some bug with backbone.localstorage
	#same in data_helpers
	getSaveData: ->
		nodes = []
		for n in @nodes.models
			nodes.push n.getSaveData()
		data =
			nodeName: @get 'nodeName'
			nodeColor: @get 'nodeColor'
			nodes: nodes


TreeNodeCollection = Backbone.Collection.extend
    model: TreeNode,
	localStorage: new Backbone.LocalStorage 'NestedList'

module.exports =
	TreeNode: TreeNode
	TreeNodeCollection: TreeNodeCollection
