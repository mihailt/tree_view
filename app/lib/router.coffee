application = require 'application'
helpers = require 'lib/data_helpers'
HeaderView = require 'views/HeaderView'
TreeRootView = require('views/TreeRootView')

TreeNodeCollection = require('models/node').TreeNodeCollection
module.exports = class Router extends Backbone.Router

	routes:
		'': 'home'

	home: =>
		vent = new Backbone.Wreqr.EventAggregator()

		headerView = new HeaderView
			vent: vent
		application.layout.header.show(headerView)

		treeRootView = new TreeRootView
			vent: vent
			collection: helpers.getTree()
		application.layout.content.show(treeRootView)

		vent.on 'top:node:added', () ->
			treeRootView.addTopLevelItem()
