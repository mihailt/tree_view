TreeNode = require('models/node').TreeNode
TreeNodeCollection = require('models/node').TreeNodeCollection

data = [
	['TURQUOISE', '#1ABC9C'],
	['GREEN SEA', '#16A085'],
	['EMERALD', '#2ECC71'],
	['NEPHRITIS', '#27AE60'],
	['PETER RIVER', '#3498DB'],
	['BELIZE HOLE', '#2980B9'],
	['AMETHYST', '#9B59B6'],
	['WISTERIA', '#8E44AD'],
	['WET ASPHALT', '#34495E'],
	['MIDNIGHT BLUE', '#2C3E50'],
	['SUN FLOWER', '#F1C40F'],
	['ORANGE', '#F39C12'],
	['CARROT', '#E67E22'],
	['PUMPKIN', '#D35400'],
	['ALIZARIN', '#E74C3C'],
	['POMEGRANATE', '#C0392B'],
	['CLOUDS', '#ECF0F1'],
	['SILVER', '#BDC3C7'],
	['CONCRETE', '#95A5A6'],
	['ASBESTOS', '#7F8C8D']
]

tree = null


getTree = () ->
	tree = loadTree()

#custom hack because some bug with backbone.localstorage
#same in node.coffee
saveTree = () ->
	saveData =[]
	for m in tree.models
		saveData.push m.getSaveData()
	saveString = JSON.stringify saveData
	localStorage.setItem 'NestedList', saveString

loadTree = () ->
	saveString = localStorage.getItem('NestedList')
	jsonData = JSON.parse(saveString);

	createModel = (data) ->
		new TreeNode(data)

	collection = new TreeNodeCollection([]);
	if jsonData
		for obj in jsonData
			collection.add createModel(obj)
	return collection


getColor = () ->
	data[ Math.floor( Math.random() * data.length )]

module.exports =
	getTree: getTree
	saveTree: saveTree
	getColor: getColor
